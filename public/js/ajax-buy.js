let buttons = document.querySelectorAll('.buy-btn');
buttons.forEach((element) => {
    element.addEventListener('click', () => {
        let id = element.getAttribute('data-id');
        (
            async () => {
                const response = await fetch('/cart/add', {
                    method: 'POST',
                    headers: {'Content-type': 'application/json;charset=utf-8'},
                    body: JSON.stringify({
                        id: id,
                    })
                });
                const answer = await response.json();
                document.getElementById('cart-count').innerText = '(' + answer.cartCount + ')';
            }
        )()
    })
})
