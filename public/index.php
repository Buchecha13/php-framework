<?php
session_start();

include_once "../vendor/autoload.php";

use app\engine\TwigRenderer;
use app\models\Cart;


$url = explode('/',$_SERVER['REQUEST_URI']);

$controllerName = $url[1] ?: 'Index';
$controllerName = "app\\controllers\\" . ucfirst($controllerName) . 'Controller';
$actionName = $url[2] ?? null;

if (class_exists($controllerName)) {
    $controller = new $controllerName(new TwigRenderer());
    $controller->runAction($actionName);
}
