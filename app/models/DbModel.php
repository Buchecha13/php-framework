<?php


namespace app\models;


use app\engine\Db;

abstract class DbModel extends Model
{
    //CRUD
    public static function getOne($id)
    {
        $tableName = static::getTableName();
        $sql = "SELECT * FROM {$tableName} WHERE id = :id";
        return Db::getInstance()->queryOne($sql, static::class, ['id' => $id]);
    }

    public static function getAll()
    {
        $tableName = static::getTableName();
        $sql = "SELECT * FROM {$tableName}";
        return Db::getInstance()->queryAll($sql, static::class);
    }

    public function insert()
    {
        $tableName = static::getTableName();
        $params = [];
        $columns = [];

        foreach ($this->properties as $key => $value) {
            $params [":{$key}"] = $this->$key;
            $columns [] = $key;
        }
        $columns = implode(', ', $columns);
        $values = implode(", ", array_keys($params));

        $sql = "INSERT INTO {$tableName} ({$columns}) VALUES ({$values})";
        Db::getInstance()->execute($sql, static::class, $params);
        $this->id = Db::getInstance()->getLastInsertId();
        return $this;
    }

    public function delete()
    {
        $tableName = static::getTableName();
        $sql = "DELETE FROM {$tableName} WHERE id = :id";
        return Db::getInstance()->execute($sql, static::class, ['id' => $this->id]);
    }

    public function update()
    {
        $tableName = static::getTableName();
        $params = [];
        $columns = [];

        foreach ($this->properties as $key => $value) {
            if (!$value) continue;
            $params [":{$key}"] = $this->$key;
            $columns [] ="{$key} = :{$key}";

            $this->properties[$key] = false;
        }

        $columns = implode(", ", $columns);
        $params['id'] = $this->id;

        $sql = "UPDATE {$tableName} SET {$columns} WHERE id = :id";

        Db::getInstance()->execute($sql, static::class, $params);
    }

    public function save()
    {
        if (is_null($this->id)) {
            $this->insert();
        } else {
            $this->update();
        }
    }
    //END CRUD

    //CUSTOM QUERY
    public static function getAllWhere($name, $value)
    {
        $tableName = static::getTableName();
        $sql = "SELECT * FROM {$tableName} WHERE `{$name}` = :value";
        return Db::getInstance()->queryAll($sql, static::class, ['value' => $value]);
    }

    public static function getOneWhere($name, $value)
    {
        $tableName = static::getTableName();
        $sql = "SELECT * FROM {$tableName} WHERE `{$name}` = :value";
        return Db::getInstance()->queryOne($sql, static::class, ['value' => $value]);
    }

    public static function getOneWhereAnd($names, $values)
    {
        $tableName = static::getTableName();
        $sql = "SELECT * FROM {$tableName} WHERE `{$names[0]}` = :value1 AND `{$names[1]}` = :value2";
        return Db::getInstance()->queryOne($sql, static::class, [
            'value1' => $values[0],
            'value2' => $values[1],
        ]);
    }
}