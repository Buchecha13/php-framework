<?php


namespace app\models;


class Order extends DbModel
{
    protected $id;
    protected $user_id;
    protected $user_name;
    protected $user_phone;
    protected $product_id;
    protected $product_price;
    protected $product_count;
    protected $total_price;


    public $properties = [
        'user_id' => false,
        'user_name' => false,
        'user_phone' => false,
        'product_id' => false,
        'product_price' => false,
        'product_count' => false,
        'total_price' => false,

    ];

    public function __construct($user_id = null, $user_name = null, $user_phone = null, $product_id = null, $product_price = null, $product_count = null, $total_price = null)
    {
        $this->user_id = $user_id;
        $this->user_name = $user_name;
        $this->user_phone = $user_phone;
        $this->product_id = $product_id;
        $this->product_price = $product_price;
        $this->product_count = $product_count;
        $this->total_price = $total_price;
    }


    public static function getTableName()
    {
        return 'orders';
    }
}