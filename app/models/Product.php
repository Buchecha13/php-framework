<?php

namespace app\models;

class Product extends DbModel
{
    protected $id;
    protected $name;
    protected $price;
    protected $description;
    protected $images;

    public $properties = [
        'name' => false,
        'price' => false,
        'description' => false,
        'images' => false,
    ];

    public function __construct($name = null, $price = null, $description = null, $images = null)
    {
        $this->name = $name;
        $this->price = $price;
        $this->description = $description;
        $this->images = $images;
    }


    public static function getTableName()
    {
        return 'products';
    }
}