<?php


namespace app\models;


class User extends DbModel
{
    protected $id;
    protected $login;
    protected $password;
    protected $hash;

    public $properties = [
        'login' => false,
        'password' => false,
        'hash' => false,
    ];

    public function __construct($login = null, $password = null, $hash = null)
    {
        $this->login = $login;
        $this->password = $password;
        $this->hash = $hash;
    }

    public function setHash()
    {
        $hash = uniqid(rand(), true);
        $this->hash = $hash;
        $this->properties['hash'] = true;
        setcookie("hash", $hash, time() + 3600, '/');
        return $this;
    }

    public static function getUserIdByLogin($login)
    {
        return $userId = self::getOneWhere('login', $login)->id;
    }


    public static function getTableName()
    {
        return 'users';
    }

    public static function auth($login, $pass, $save)
    {
        $user = User::getOneWhere('login', $login);
        if ($user) {
            if (password_verify($pass, $user->password)) {
                if ($save) {
                     $user->setHash()->save();
                }
                $_SESSION ['auth']['login'] = $login;
                $_SESSION ['auth']['id'] = $user->id;
                return true;
            }
        } else {
            return false;
        }
    }

    public static function isAuth()
    {
        if (isset($_COOKIE['hash'])) {
            $hash = $_COOKIE['hash'];
            $user = self::getOneWhere('hash', $hash);
            if ($user) {
                $_SESSION['auth']['login'] = $user->login;
            }
        }
        return isset($_SESSION['auth']['login']);
    }

    public static function getName()
    {
            return $_SESSION['auth']['login'] ?? null;
    }
}