<?php


namespace app\models;


use app\engine\Db;

class Cart extends DbModel
{
    protected $id;
    protected $product_id;
    protected $user_cart_id;
    protected $product_count;
    protected $user_id;

    protected $properties = [
        'product_id' => false,
        'user_cart_id' => false,
        'product_count' => false,
        'user_id' => false,
    ];


    public function __construct($product_id = null, $user_cart_id = null, $user_id = null, $product_count = 1)
    {
        $this->product_count = $product_count;
        $this->user_id = $user_id;
        $this->product_id = $product_id;
        $this->user_cart_id = $user_cart_id;
    }

    public static function updateProductCountByUserCartId($userCartId, $productId)
    {

        $product = Cart::getOneWhereAnd(['user_cart_id', 'product_id'], [$userCartId, $productId]);

        if ($product) {
            $product->product_count++;
            $product->properties['product_count'] = true;
            $product->save();

            return true;
        }
        return false;
    }

    public static function updateProductCountByUserId($userId, $productId)
    {

        $product = Cart::getOneWhereAnd(['user_id', 'product_id'], [$userId, $productId]);

        if ($product) {
            $product->product_count++;
            $product->properties['product_count'] = true;
            $product->save();

            return true;
        }
        return false;
    }

    public static function getCartByUserCartId($userCartId)
    {
        $sql = "SELECT products.id, products.name, products.description, products.price, products.images, product_count FROM  `carts`
                INNER JOIN `products` ON carts.product_id = products.id
                WHERE `user_cart_id` = '{$userCartId}'";
        return Db::getInstance()->queryAll($sql, static::class);
    }

    public static function getCartByUserId($userId)
    {
        $sql = "SELECT products.id, products.name, products.description, products.price, products.images, product_count FROM  `carts`
                INNER JOIN `products` ON carts.product_id = products.id
                WHERE `user_id` = '{$userId}'";
        return Db::getInstance()->queryAll($sql, static::class);
    }

    public static function getUserCartCountById($userId)
    {
        if (User::isAuth()) {
            $sql = "SELECT SUM(product_count) as count FROM `carts` WHERE `user_id`=:user_id";
            return (Db::getInstance()->queryOne($sql, static::class, ['user_id' => $userId]))->count;
        }
        $sql = "SELECT SUM(product_count) as count FROM `carts` WHERE `user_cart_id`=:user_cart_id";
        return (Db::getInstance()->queryOne($sql, static::class, ['user_cart_id' => $userId]))->count;
    }

//  Устанавливаем идентификатор пользователя-гостя, для определения состава его корзины.
//  Время жизни куки = время жизни корзины
    public static function setUserCartKey()
    {
        $cartKey = uniqid('cart_', true);
        setcookie('user_cart_id', $cartKey, time() + 3600, '/');

        return $cartKey;
    }


    public static function getTableName()
    {
        return 'carts';
    }
}