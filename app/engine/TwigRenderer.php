<?php


namespace app\engine;


use app\interfaces\IRenderer;

class TwigRenderer implements IRenderer
{

    public function render($template, $params)
    {
        $loader = new \Twig\Loader\FilesystemLoader('../views');

        $twig = new \Twig\Environment($loader, [
            'debug' => true,
        ]);

        echo $twig->render($template, $params);
    }
}