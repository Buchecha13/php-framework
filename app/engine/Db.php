<?php

namespace app\engine;


use app\models\Product;
use app\traits\TSingleTone;

class Db
{
    use TSingleTone;

    protected $config = [
        'driver' => 'mysql',
        'host' => '127.0.0.1:3310',
        'login' => 'root',
        'password' => 'root',
        'database' => 'php-2',
        'charset' => 'utf8'
    ];

    private $connection = null; // объект PDO

    private function getConnection()
    {
        if (is_null($this->connection)) {
            $this->connection = new \PDO(
                $this->prepareDsnString(),
                $this->config['login'],
                $this->config['password']);
            $this->connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        }
        return $this->connection;
    }

    private function prepareDsnString()
    {
        return sprintf("%s:host=%s;dbname=%s;charset=%s",
            $this->config['driver'],
            $this->config['host'],
            $this->config['database'],
            $this->config['charset']
        );
    }

    public function getLastInsertId()
    {
        return $this->getConnection()->lastInsertId();
    }

    private function query($sql, $className, $params)
    {
        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute($params);

        $stmt->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $className);
        return $stmt;
    }

    public function queryOne($sql, $className, $params = [])
    {
        return $this->query($sql, $className, $params)->fetch();
    }

    public function queryAll($sql, $className, $params = [])
    {
        return $this->query($sql, $className, $params)->fetchAll();
    }

    public function execute($sql, $className, $params = [])
    {
        return $this->query($sql, $className, $params)->rowCount();
    }
}