<?php


namespace app\controllers;


use app\models\Cart;
use app\models\Product;
use app\models\User;

class CartController extends Controller
{
    public function actionIndex()
    {
        if (User::isAuth()) {
            $userId = (User::getOneWhere('login', $_SESSION['auth']['login']))->id;

            $cartProducts = Cart::getCartByUserId($userId);
        } else {
            $cartProducts = Cart::getCartByUserCartId($_COOKIE['user_cart_id']);
        }

        return $this->render('cart.twig', [
            'cartProducts' => $cartProducts,
        ]);
    }

    //Получился слишком "толстый" экшн,
    //так как в заивисимости от роли пользователя, изменяются действия (гость, или авторизованный).
    //TODO Можно доработать
    public function actionAdd()
    {
        $userId = null;
        $userCartId = null;

        $data = json_decode(file_get_contents('php://input'));
        $productId = $data->id;

//      Определение кто добавляет товарв корзину - авторизованный пользователь, или гость
        if (User::isAuth()) {
            $userId = User::getUserIdByLogin($_SESSION['auth']['login']);
//          Если пользователь уже добавлял такой товар,
//          то просто обновим значение count для уже существующий записи в бд
            if (Cart::updateProductCountByUserId($userId, $productId)) {
                $response = [
                    'success' => 'ok',
                    'cartCount' => Cart::getUserCartCountById($userId),
                ];

                echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
                die();
            }
        } else if (($_COOKIE['user_cart_id'])) {
            $userCartId = $_COOKIE['user_cart_id'];

            if (Cart::updateProductCountByUserCartId($userCartId, $productId)) {
                $response = [
                    'success' => 'ok',
                    'cartCount' => Cart::getUserCartCountById($userCartId),
                ];

                echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
                die();
            }
//      Если зашел гость и добавляет товар первый раз - присвоим ему уникальный идентификатор корзины
        } else {
            $userCartId = Cart::setUserCartKey();
        }
//      Создание записи в таблице корзин, если такой продукт еще не был добавлен пользователем в корзину
        (new Cart($productId, $userCartId, $userId))->save();

        $response = [
            'success' => 'ok',
            'cartCount' => Cart::getUserCartCountById($userId) ?? Cart::getUserCartCountById($userCartId),
        ];

        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        die();
    }

    //TODO Можно сделать асинхронно
    public function actionDelete()
    {
        $productId = $_POST['product_id'];
        if (User::isAuth()) {
            $userId = (User::getOneWhere('login', $_SESSION['auth']['login']))->id;

            $cartProduct = Cart::getOneWhereAnd(['product_id', 'user_id'], [$productId, $userId]);
            $cartProduct->delete();
        } else {
            $useCartId = $_COOKIE['user_cart_id'];
            $cartProduct = Cart::getOneWhereAnd(['product_id', 'user_cart_id'], [$productId, $useCartId]);
            $cartProduct->delete();
        }

        header('Location:' . $_SERVER['HTTP_REFERER']);
        die();
    }
}