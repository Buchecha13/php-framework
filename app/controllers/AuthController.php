<?php


namespace app\controllers;


use app\models\User;

class AuthController extends Controller
{
    public function actionLogin()
    {
        $login = $_POST['login'];
        $pass = $_POST['password'];
        $save = $_POST['save'];
        if (User::auth($login, $pass, $save)) {
            header('Location:' . $_SERVER['HTTP_REFERER']);
            die();
        } else {
            $_SESSION['auth']["error"] = 'Неверный логин или пароль';
            header('Location:' . $_SERVER['HTTP_REFERER']);
            die();
        }

    }

    public function actionLogout()
    {
        session_destroy();
        setcookie('hash', null, -1, '/');
        header('Location:' . $_SERVER['HTTP_REFERER']);
        die();
    }
}