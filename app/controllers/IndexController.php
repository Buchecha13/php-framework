<?php


namespace app\controllers;


class IndexController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index.twig');
    }

}