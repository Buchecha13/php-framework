<?php


namespace app\controllers;


use app\models\Product;

class CatalogController extends Controller
{

    public function actionIndex()
    {
        $products = Product::getAll();

        return $this->render('catalog.twig', [
            'products' => $products,
        ]);
    }

    public function actionCard()
    {
        $id = $_GET['id'];
        $product = Product::getOne($id);

        return $this->render('product_card.twig', [
            'product' => $product,
        ]);
    }
    
}