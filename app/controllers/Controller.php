<?php


namespace app\controllers;


use app\interfaces\IRenderer;
use app\models\Product;
use app\models\User;
use app\models\Cart;

class Controller
{
    private $action;
    private $defaultAction = 'Index';
    private $renderer;


    /**
     * Controller constructor.
     */
    public function __construct(IRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function runAction($action)
    {
        $this->action = $action ?? $this->defaultAction;
        $method = 'action' . ucfirst($this->action);
        if (method_exists($this, $method)) {
            $this->$method();
        }
    }

    public function render($templates, $params = [])
    {
        $userId = null;
        if (User::isAuth()) {
            $userId = User::getUserIdByLogin($_SESSION['auth']['login']);
        } else if ($_COOKIE['user_cart_id']) {
            $userId = $_COOKIE['user_cart_id'];
        }

        $defaultParams = [
            'isAuth' => User::isAuth(),
            'userName' => User::getName(),
            'message' => $_SESSION['auth']['error'],
            'cartCount' => Cart::getUserCartCountById($userId) ?? 0,
        ];

        $params = array_merge($params, $defaultParams);
        unset($_SESSION['auth']["error"]);
        return $this->renderer->render($templates, $params);
    }
}