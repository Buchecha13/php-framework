<?php


namespace app\traits;


trait TSingleTone
{
    private static $instance = null;//Объект DB

    private function __construct() {}
    public function __wakeup() {
        throw new Exception("Cannot unserialize singleton");
    }
    private function __clone() {}

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

}